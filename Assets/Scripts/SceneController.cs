﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

	private void OnGUI() {
		Event e = Event.current;

		if(e.type == EventType.KeyDown && e.keyCode == KeyCode.Escape) {
			Application.Quit();
		}
	}
}
